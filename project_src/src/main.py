#!/usr/bin/env python

import rospy
import time
from cylinder_manipulation import *
from arm_manipulation import *
from robot_manipulation import *

class TableCleaner():

	def __init__(self):
		#initialise
		rospy.init_node('detector', anonymous=True, log_level=rospy.INFO)

		self.moveBase = GoToPose()
		self.Move_Arm = Move_arm()
		self.canPose = None
		self.detect = None

		self.binPose =   {'x':-2.20, 'y':8.25,  'q1':0.0, 'q2':0.0, 'q3':1.0, 'q4':0.0 }
		self.tablePose = {'x': 0.00, 'y':0.00,  'q1':0.0, 'q2':0.0, 'q3':0.0, 'q4':1.0 }

	def run(self):
		#pick up the can
		picked = self.pickCan()

		#if can was not picked stop the execution
		if not picked:
			print("did not pick a can")
			return False

		weight = self.weightCan()
		print("Estimated can weight: %.0f g" % weight)

		throwAway = self.decide(weight,20)
		if(throwAway):
			#return to the table
			self.moveBase.goto(self.tablePose['x'], self.tablePose['y'], 0.0,
								self.tablePose['q1'], self.tablePose['q2'], self.tablePose['q3'],self.tablePose['q4'])

	def moveToBin(self,binPose):
		#position arm for base movement
		self.Move_Arm.predefinedMove("home",10)
		#go to bin
		self.moveBase.goto(binPose['x'], binPose['y'], 0.0,
							binPose['q1'], binPose['q2'], binPose['q3'],binPose['q4'])
		#extend arm to throw away the can
		self.Move_Arm.predefinedMove("extend",5)
		#drop the can into the bin
		self.Move_Arm.predefinedMove("open",5)
		#position arm for base movement
		self.Move_Arm.predefinedMove("home",10)

	def pickCan(self):
		#moe arm to suitable position for picking object on the table
		rospy.loginfo("Move arm to pregrasp position.")
		self.Move_Arm.predefinedMove("pregrasp",20)
		self.Move_Arm.predefinedMove("open",10)

		#look around to have better knowledge of suroundings
		rospy.loginfo("Inspect suroundings.")
		self.Move_Arm.predefinedMove("look_around",10)

		#detect can
		try:
			self.detect = Detector()
			self.detect.startCanDetect()
			#wait until a can is found
			while self.detect.canPose == None:
				time.sleep(1)

			self.detect.stopCanDetect()

		except Exception as e:
			print(e)

		#rospy.sleep(1)
		self.canPose = self.detect.canPose
		#set can pose detector back to none
		self.detect.canPose = None

		print(self.canPose)
		#pick up the can
		picked = self.Move_Arm.pickCan(self.canPose)

		return picked

	def weightCan(self):
		#get sensor data to estimate can weight
		self.detect.getFTSensorData()
		while self.detect.ftData == None:
			time.sleep(1)

		self.detect.ftData_sub.unregister()
		sensorData = self.detect.ftData
		#subtrack the aproximate sensor reading for empty gripper
		sensorData.x -= 11.52
		#modify data to show weight in grams
		weight = sensorData.x * 100
		return weight

	def decide(self,weight,limit):
		#if weight is more than 20g
		if weight > limit:
			#put can back on the table
			self.canPose.z -= 0.1
			self.Move_Arm.moveArm(self.canPose)
			rospy.sleep(1)
			#open gripper and move gripper above the can to avoid hitting it
			self.Move_Arm.predefinedMove("open",5)
			self.canPose.z += 0.25
			self.Move_Arm.moveArm(self.canPose)
			#return hand to home position
			self.Move_Arm.predefinedMove("home",10)
			return False
		else:
			#throw away the can
			self.moveToBin(self.binPose)
			return True


def main():
	cleaner = TableCleaner()
	cleaner.run()
if __name__ == '__main__':
	main()
