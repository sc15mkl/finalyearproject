
import rospy
import sys
import tf
import moveit_commander
import actionlib

import geometry_msgs.msg
from play_motion_msgs.msg import PlayMotionAction, PlayMotionGoal
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PoseStamped, WrenchStamped

class Move_arm():

	def __init__ (self):
		self.canPose = None
		moveit_commander.roscpp_initialize(sys.argv)
		group_name = "arm_torso"
		self.group = moveit_commander.MoveGroupCommander(group_name)
		self.robot = moveit_commander.RobotCommander()
		self.scene = moveit_commander.PlanningSceneInterface()
		self.group.set_planning_time(15)
		self.client = actionlib.SimpleActionClient("play_motion", PlayMotionAction)
		self.client.wait_for_server()
		self.group.clear_path_constraints()

	def moveArm (self,position):
		pose_goal = self.getPose(position)
		self.group.set_pose_target(pose_goal)
		move = self.group.go(wait=True)
		self.group.stop()
		return move

	def pickCan (self, canPose):
		#move infornt of the can
		canPose.x -= 0.3
		move = self.moveArm(canPose)
		if not move:
			return move
		#move forward for grapping
		canPose.x += 0.15
		move = self.moveArm(canPose)
		if not move:
			return move
		#close gripper
		self.predefinedMove("close_half",5)
		#lift the can
		canPose.z += 0.1
		move = self.moveArm(canPose)
		if not move:
			return move
		return True

	def predefinedMove(self,moveName,waitTime):
		rospy.wait_for_message("joint_states", JointState)
		rospy.sleep(1.0)
		rospy.loginfo("Start motion [" + moveName + "]...")
		goal = PlayMotionGoal()
		goal.motion_name = moveName
		goal.skip_planning = False

		self.client.send_goal(goal)
		self.client.wait_for_result(rospy.Duration(waitTime))
		rospy.loginfo("...Motion finished.")

	def getPose(self,position):
		pose = PoseStamped()
		pose.header.frame_id = "base_footprint"
		pose.pose.position = position
		quaternion = tf.transformations.quaternion_from_euler(-1.57, 0, 0)
        	pose.pose.orientation.x = quaternion[0]
        	pose.pose.orientation.y = quaternion[1]
        	pose.pose.orientation.z = quaternion[2]
        	pose.pose.orientation.w = quaternion[3]

		pose_goal = geometry_msgs.msg.Pose()
		pose_goal.orientation = pose.pose.orientation
		pose_goal.position = pose.pose.position
		return pose_goal
