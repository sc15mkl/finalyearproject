#!/usr/bin/env python

import rospy
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Pose, Point, Quaternion
from actionlib_msgs.msg import *

class GoToPose():
    def __init__(self):

        #create action client
    	self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)

        #allow up to 5 seconds for the action server to come up
    	self.move_base.wait_for_server(rospy.Duration(5))

    def goto(self,x,y,z,q1,q2,q3,q4):
    	goal = MoveBaseGoal()
        
    	goal.target_pose.header.frame_id = 'map'
    	goal.target_pose.header.stamp = rospy.Time.now()
    	goal.target_pose.pose = Pose(Point(x, y, z),Quaternion(q1, q2, q3, q4))

    	#start moving
    	self.move_base.send_goal(goal)
        #allow up to a minute to complete task
    	success = self.move_base.wait_for_result(rospy.Duration(60))

    	state = self.move_base.get_state()
    	result = False
    	if success and state == GoalStatus.SUCCEEDED:
    		#goal reached
    		result = True
    	else:
    		self.move_base.cancel_goal()

        return result
