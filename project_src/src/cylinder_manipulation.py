#!/usr/bin/env python

import rospy
import roslaunch

from geometry_msgs.msg import PoseStamped, WrenchStamped

class Detector():

	def __init__ (self):
		self.canPose = None
		self.cloud_sub = None
		self.ftData = None
		self.ftData_sub = None
		print("test")
		uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
		roslaunch.configure_logging(uuid)
		self.launch = roslaunch.parent.ROSLaunchParent(uuid, ["/home/csunix/sc15mk/Project_ws/src/project_src/launch/cylinder_detector.launch"])

	def startCanDetect (self):
		self.launch.start()
		rospy.sleep(1)
		self.cloud_sub = rospy.Subscriber("/cylinder_detector/cylinder_pose",
						PoseStamped,
						self.getPose)

	def getFTSensorData(self):
		self.ftData_sub = rospy.Subscriber("wrist_ft",
						WrenchStamped,
						self.getftData)

	def getPose(self, PoseStamped):
		#update the can position
		self.canPose = PoseStamped.pose.position

	def getftData(self, WrenchStamped):
		self.ftData = WrenchStamped.wrench.force

	def stopCanDetect(self):
		self.launch.shutdown()
		self.cloud_sub.unregister()
