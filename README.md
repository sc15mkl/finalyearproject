## About
This package contains exploratory software that looks into how TIAGo robot could be able to estimate object mass using the force/torque sensor that is located in robot's wrist.

## How to install

This project followed [this](http://wiki.ros.org/Robots/TIAGo/Tutorials/Installation/InstallUbuntuAndROS) guide to install ROS. 

After you install ROS you can then proceed to install TIAGo simulation using [this](http://wiki.ros.org/Robots/TIAGo/Tutorials/Installation/TiagoSimulation) guide (if you don't see any content make sure you select kinetic version).
It installs all the necessary packages that are required for this project to run.

To install this project's package, clone the repository into the src folder of the workspace:
```
git clone https://gitlab.com/sc15mkl/finalyearproject.git
```

return to the workspace and run this command:
```
catkin_make
```


Before you can run the simulation you will need to modify a file located in `project_src/src/cylinder_manipulation.py`
In line 18 change file link to contain correct full path to the `cylinder_detector.launch` file. `*path_to_workspace*/*workspace*/src/project_src/launch/cylinder_detector.launch`

## How to run

To run the demonstration you will need two separate command line terminals with workspace initialised.

To initialise workspace navigate into the workspace directory and run this command:
```
source devel/setup.bash
```
To launch the simulation run this command:
```
roslaunch project_src project.launch
```
To run the demonstration run this command in separate command line terminal with initalised workspace:
```
rosrun project_src main.py
```
